package com.wang.ddnsdemo2;

import com.aliyuncs.exceptions.ClientException;
import com.wang.ddnsdemo2.utils.IpUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

import java.util.Calendar;
import java.util.Date;

@SpringBootApplication
public class DdnSdemo2Application {

    public static void main(String[] args) throws IOException, ClientException, InterruptedException {
        SpringApplication.run(DdnSdemo2Application.class, args);
        while (true)
        {
            Date date = new Date();
            System.out.println("[ping]"+"当前时间"+date.toString());
            Thread.sleep(60*1000);
            IpUtil.main(args);
            System.out.println("[pong]"+"当前时间"+date.toString());
            System.out.println();
        }
    }
}
