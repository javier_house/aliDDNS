package com.wang.ddnsdemo2.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import java.io.IOException;

@Component
public class IpUtil {
    //AccessKeyID AccessKeySecret RecordID(需要修改的ID) Yuming（需要修改的域名）
    private static String AccessKeyID;
    private static String AccessKeySecret;
    private static String RecordID;
    private static String Yuming;

//    public void getKey()
//    {
//        System.out.println("AccessKeyID:"+AccessKeyID);
//        System.out.println("AccessKeySecret:"+AccessKeySecret);
//    }

    // 获取IP
    public static String ipGet() throws IOException {
        Connection connection = Jsoup.connect("https://2022.ipchaxun.com/").ignoreContentType(true);
        JSONObject parse = JSON.parseObject(connection.execute().body());
        return parse.get("ip").toString();
    }

    // 获取现在的阿里云解析的IP
    public static String aliIpGet()
    {
        //地域：默认
        DefaultProfile defaultProfile = DefaultProfile.getProfile("default",AccessKeyID,AccessKeySecret);
        IAcsClient client = new DefaultAcsClient(defaultProfile);
        CommonRequest request = new CommonRequest();
        request.setMethod(MethodType.POST);
        request.setDomain("alidns.aliyuncs.com");
        request.setVersion("2015-01-09");
        request.setAction("DescribeDomainRecords");
        request.putQueryParameter("DomainName",Yuming);
        try {
            CommonResponse response = client.getCommonResponse(request);
            String str =response.getData();
            JSONObject jsonObject = JSON.parseObject(str);
            JSONObject domainRecords =jsonObject.getJSONObject("DomainRecords");
            JSONArray record =domainRecords.getJSONArray("Record");
            JSONObject result = record.getJSONObject(0);
//            Start.RecordId=result.getString("RecordId");//取得更改需要的解析记录ID
            return result.getString("Value");//取得已经存在的IP地址
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    // 是否需要修改
    public static void direct() throws IOException, ClientException {
        if(!IpUtil.ipGet().equals(aliIpGet()))
        {
            DefaultProfile profile = DefaultProfile.getProfile("default",AccessKeyID,AccessKeySecret);
            IAcsClient client = new DefaultAcsClient(profile);
            CommonRequest request = new CommonRequest();
            request.setMethod(MethodType.POST);
            request.setDomain("alidns.aliyuncs.com");
            request.setVersion("2015-01-09");
            request.setAction("UpdateDomainRecord");
            request.putQueryParameter("RecordId",RecordID);//放入参数解析记录ID
            request.putQueryParameter("RR", "*");
            request.putQueryParameter("Type", "A");
            request.putQueryParameter("Value",IpUtil.ipGet());//放入参数新的IP地址
            CommonResponse response = client.getCommonResponse(request);
        }
    }


    public static void main(String[] args) throws IOException, ClientException {
        System.out.println("本地ip:"+IpUtil.ipGet());
        System.out.println("阿里云ip:"+aliIpGet());
        IpUtil ipUtil = new IpUtil();
//        ipUtil.getKey();
        direct();

    }

    //SpringBoot 获取自定义 property
    @Value("${demo.accessKeySecret}")
    public void setAccessKeySecret(String accessKeySecret)
    {
        this.AccessKeySecret = accessKeySecret;
    }

    @Value("${demo.accessKeyID}")
    public void setAccessKeyID(String accessKeyID)
    {
        this.AccessKeyID = accessKeyID;
    }

    @Value("${demo.recordID}")
    public void setRecordID(String recordID)
    {
        this.RecordID = recordID;
    }

    @Value("${demo.yuming}")
    public void setYuming(String yuming)
    {
        this.Yuming = yuming;
    }
}
